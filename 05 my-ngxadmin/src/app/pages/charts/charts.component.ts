import { Component } from '@angular/core';

// import { echarts } from 'echarts';
// declare const echarts: any;

@Component({
  selector: 'ngx-charts',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ChartsComponent {
}
