/*app.js */
/*
 @author      Juan Luna
 @description un servidor para la app avr3ds y rutas con controllers, conexion a bd
              y modelos, db/DB_avr3ds
 @scripts
              "scripts": {
              "ng": "ng",
              "start": "ng serve ",
              "build": "ng build --prod",
              "test": "ng test",
              "lint": "ng lint",
              "e2e": "ng e2e",
              "server": "nodemon server.js"
    }
 @DB_conexion mongod –dbpath C:/xampp/htdocs/_NODE/data/dbs/   --port 27017
 @DB_name     DB_avr3ds

 @nodeModules
 instaled:
 var express  = require("express");                  //express
 var path = require('path');                         //path
 var logger = require('morgan');                     //colores de console
 var bodyParser = require('body-parser');            //body y formualrios
 var cnnectFlash = require('connect-flash);          //para storing messages (de momento no lo estás usando)
 var cokieParser = require('cookie-parser');         //cookies, necesario para express-session y passport
 var swig = require('swig');                         //motor plantillas html
 var session  = require('express-session');          //sesion de express necesario para passport
 var passport = require('passport');                 //para auth login redes sociales
 var passLocal= require('passport-local');           //authenticacion en bd local
 var passfacebook= require('passport-facebook');     //authenticacion en facebook
 var passTwitter= require('passport-twitter');       //authenticacion en twitter
 var formidable= require('formidable');              //para subir ficheros desde el usuario
 var qt = require('quickthumb');                     //para ver imagenes formatos y thumbnails
 var mongoose = require('mongoose');                 //para conexion y tratamiento base de datos mongodb
 var methodOverride = require("method-override")     //para poder usar post, get, put y delete en CRUDS actions
 var flash = require('flash-connect');               //para mensajes de error en el frontend
 var jwt = require('jtw-simple');                    //genera tokens
 var moment = require('moment');                     //trabaja con fecha en formato unix

 not-installed:
 //var multer = require('multer');                       //multipart form data subida de ficheros. Tambien puedes usar  multipart o formidable
 //var errorHandler = require('errorhandler');           //errores
 //var favicon = require('server-favicon');               //fav-icon

 //var redis = require('redis-connect');                 //para conexiones con redis que guarda sesiones incluso despues de cortar el servidor

 //var fs          = require('fs');                      //para renombrar el upload_
 //var passport    = require('passport');
 //var slugs       = require('slugs');
 //var S           = require('string');                  //metodos con strings
 //var crypto      = require('crypto');                  //encriptar contraseñas
 //var async       = require('async');                    //realiza operaciones con funciones controlando la sincronizacion por ejemplo async.waterfall()

 //var http = require('http');                           //modulo de node lo utilizaremos con socket.io

 */
//##### modulos requeridos -----------------------------------------------------
//try {
//de configuracion y propios
// var config = require('./app_server/config/general');                          //config.general
// var r_mod = config.ROUTE_MODULES;                                            //rutas de node_modules
// var sessec = config.PASSPORT_SECRET;                                          //para session passport

//modulos externos de npm
// var express = require(r_mod + "express");                               //express
var express = require('express');
// var path = require('path');                                          //path
// var logger = require(r_mod + 'morgan');                                //colores de console
// var bodyParser = require(r_mod + 'body-parser');                           //body y formualrios
var bodyParser = require('body-parser');
// var cookieParser = require(r_mod + 'cookie-parser');                         //cookies, necesario para express-session y passport
// var swig = require(r_mod + 'swig');                                  //motor de plantillas
// var session = require(r_mod + 'express-session');                       //sesion de express necesario para passport
// var passport = require(r_mod + 'passport');                              //para auth login redes sociales

//var methodOverride = require(r_mod + "method-override");                      //para poder usar post, get, put y delete en CRUDS actions
//##### modulos requeridos ----------------------------------------------------- FIN



//##### configuracion express --------------------------------------------------
var app = express(); //controlador principal del servidor de node

// server.set('port', process.env.PORT || 3000);                                   //puerto en development
// server.set('env', process.env.NODE_ENV || 'development');                       //por defecto este será el env
// server.set('logs', true);                                                       //podemos poner true o false, para que escriba logs en ficheros
//configuracion de rutas dependiendo de si estamos en prod o en devel

//if(server.get('env') === 'production' || server.get('env') === 'prod' ){
//    var port = server.get('port');
//    var config_url = require('./app_server/config/remote')(port);      //.URLS.MEDIA_URL//configuracion de rutas y demás en produccion y development
//    media_url = config_url.MEDIA_URL;
//    temp_url  = config_url.TEMP_UPLOADS_URL;
//}else if(server.get('env') === 'development' || server.get('env') === 'dev' ){
// var port = server.get('port');
// var config_url = require('./app_server/config/urls')(port);      //.URLS.MEDIA_URL//configuracion de rutas y demás en produccion y development
// var media_url = config_url.MEDIA_URL;
// var temp_url = config_url.TEMP_UPLOADS_URL;
//configuracion propia de base_url para mandarla a extra-data por ejemplo
// server.set('base_url', config_url.BASE_URL);
//console.log("####### config_URLS #######" + media_url);
//}

// algunos modulos MIDDLEWARES
// server.use(logger('dev'));                                                      //color to console errors
// server.use(express.static('./app_browser/'));                                   //  './public'//carpeta static del proyect
app.use(bodyParser.urlencoded({
    extended: false
})); //el servidor aceptará datos por post
app.use(bodyParser.json()); //para que esos datos vayan en formato json
// //server.use(methodOverride());
// //req.MEDIA_URL
// //req.TEMP_UPLOADS_URL
// require('./app_server/config/uploads')(server, media_url, temp_url);            //configura la req.MEDIA_URL,
// //para subida de ficheros  al servidor
// //desde un formulario
// server.use(function (req, res, next) {                                           //cabeceras necesarias para manejar tokens y algo de seguridad
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
//     next();
// });
//##### configuracion express -------------------------------------------------- FIN
//
//##### configuracion de swig --------------------------------------------------
// server.set('view engine', 'html');
// server.set('views', __dirname + '/app_server/views');
// server.engine('html', swig.renderFile);
// swig.setDefaults({cache: false});
//##### configuracion de swig -------------------------------------------------- FIN
//
////##### configuracion passport -----------------------------------------------
// server.use(cookieParser());                                                     //estos dos son necesarios para passport
// server.use(session({secret: config.PASSPORT_SECRET}));                          //el secret puede ser lo que tu quieras, para poder usar sesiones necesitamos cookies
// //
// server.use(passport.initialize());                                              //metodos necesarios para el logueo con passport
// server.use(passport.session());                                                 //requiere sessiones de express express-session
// ////serializzacion del usuario con passport
// passport.serializeUser(function (user, done) {
//     done(null, user);
// });                                                                             //serializando la session del usuario para peticiones de autenticacion, se guardará en una variable llamada req.user
// ////deserializacion user
// passport.deserializeUser(function (user, done) {
//     done(null, user);
// });
//##### configuracion passport ------------------------------------------------- FIN
//
////##### RUTAS ----------------------------------------------------------------
// importacion de rutas
var mainRoutes = require("./app_server/routes/main.routes.js");
var userRoutes = require("./app_server/routes/user.routes.js");
var loginRoutes = require("./app_server/routes/login.routes.js");
// #### resolver rutas !!ojo cuidao deben respetar el orden
app.use('/user', userRoutes);
app.use('/login', loginRoutes);
app.use('/', mainRoutes);
////##### RUTAS ---------------------------------------------------------------- FIN

// require('./app_server/controllers/pageController/homeCtrl')(server);            //acceso a /, /about, /contact, etc
// require('./app_server/controllers/usersControllers/usersCtrl')(server);          //usuarios de la app (server,io)cuando el socket este habilitado
// require('./app_server/controllers/coursesController/coursesCtrl')(server);      //programas de tv  /(app,io) cuando el socket este habilitado
// require('./app_server/controllers/discussControllers/commentsCtrl')(server);    //cuando se suben  comentarios sobre un curso o capitulo
// require('./app_server/controllers/faqsController/faqsCtrl')(server);    //cuando se suben  comentarios sobre un curso o capitulo
// require('./app_server/controllers/blogController/blogCtrl')(server);            //para extraer ls articulos oi el articulo en el futro para subirlos tambien
// require('./app_server/controllers/discussControllers/foroCtrl')(server);        //foros y posts
// require('./app_server/controllers/rootController/rootCtrl')(server);            // datos genericos para rootScope como categories, tags, likes ...+
// require('./app_server/controllers/discussControllers/postsCtrl')(server);       // los posts de los foros
// require('./app_server/controllers/likesController/likesCtrl')(server);          //controla los likes en diferentes objetos
// require('./app_server/controllers/usersControllers/auth_tipicallyCtrl')(server);//para login or signin sin twitter o facebook
// require('./app_server/controllers/usersControllers/profileCtrl')(server);       //profile users datos para el profile
// require('./app_server/controllers/_gt3wControllers/adminCtrl')(server);         //profileadmin:
// require('./app_server/controllers/_gt3wControllers/conversationCtrl')(server);  //profile users & admin: questions, messages, answers
// require('./app_server/controllers/_gt3wControllers/approbationCtrl')(server);    //profileadmin: aprovaciones de foros y comments
// require('./app_server/controllers/_gt3wControllers/approbationCtrl')(server);    //profileadmin: aprovaciones de foros y comments
//
////require('./app_server/controllers/chatCtrl')(server,io);                    //sistema de discusiones preguntas y respuestas
//require('./app_server/controllers/effectControllers/effectsCtrl')(server);    //solo para ver ciertos posibles efectos que podriamos meter en la pagina
////pruebas con angular
//require('./app_server/controllers/apiControllers/apiCtrl')(server);           //para api con angular
////##### controllers ---------------------------------------------------------- FIN
////
////### connections ------------------------------------------------------------
// require('./app_server/connections/facebook')(server);                           //nueva forma de pasar el express a un controlador
// require('./app_server/connections/twitter')(server);                            //las conexiones son especie de middlewares
// require('./app_server/connections/local-login')(server);                        //authenticacion local
////### connections ------------------------------------------------------------ FIN

//    //los errores de recursos de la carpeta public no se consideran errores en el servidor
//##### tratamiento de errores -------------------------------------------------
// var errorHandler = require('./app_server/classes/errorHandler');
// // catch 404 and forward to error handler
// server.use(function (err, req, res, next) {
// //        console.log(err);
//     //si el problema se inicio en el cliente
//     if (err instanceof errorHandler){
//         err.actiON();//type, message, status, origin
//     }else{
//         err = new errorHandler(err.name, err.message);
//         err.actiON();
//     }

//     console.log("****************** ERROR CONTINUE: 1");
//     if(req.xhr){
//         err.type    = err.type   || 'user exception';
//         err.origin  = err.origin || 'XHR-client';
//         err.status  = err.status || 404;
//     }else{
//         err.type    = err.type   || 'server exception';
//         err.origin  = err.origin || 'server-internal';
//         err.status  = err.status || 500;
//     }
//     console.log(err.getLogConsole());//type, message, status

//     next(err);
// });

// //control de errores con la clase handleErrors
// server.use(function (err, req, res, next) {
//     //si hay usuario logged
//     console.log("****************** ERROR CONTINUE: 2");
//     if(req.user){
//         err.origin += ', ' + req.user.username;
//     }
//     //si express ya envio una respuesta
//     if(res.headersSent){
//         console.log("express ya envio una respuesta");
// //            next();
//     }//no se si esto sera necesario
//     //in development renderizamos
//     if (server.get('env') === 'development') {
// //            console.log("development");
//         console.log(err.getLogExtra());
//         console.log('**************************** ERROR FINISH');
//            console.log(err.stack);
//            res.status(err.status || 500);//si ponemos esto no se envia el render o el json
//     if(req.xhr){
//         console.log("se envia un json({message, err}) al client");
//         res.json({
//             message: err.message,
//             error: err
//         });
//     }else{
//         console.log("no se envia nada al client");
//     }

// } else{//server.get('env') === 'production')
//     //in production logs file
//            res.status(err.status || 500);//si ponemos esto no se envia el render o el json
//         console.log("production");
//         console.error(err.getLogExtra()); //date, origin, type, message, status
// //            console.log(err.stack);
//         console.error('****************************** ERROR FINISH');
//             //escritura de logs error controlado
//             if (server.get('logs') === true){
//                 //escribir en fichero logs también
//                 //var line_log = err.getLogExtra + err.getLogConsole(' | ') + '\n\r';//con separador para file y salto de linea
// //                  //#### openfile(/logs/error_logs.txt), writeFile(line_log);
//             }else{
//                 console.log("Logs File no esta activado");
//             }

//         if(req.xhr){
//             console.log("se envia un json({message}) al client");
//             res.json({
//                 message: err.message,
//                 error: {}
//             });
//         }else{
//             console.log("no se envia nada al client");
//         }
//     }
// });

//##### tratamiento de errores ------------------------------------------------- FIN
// var loggs = require('./app_server/classes/loggs');                          //escritura de errores en pantalla

// ##### puerto de escucha del server ------------------------------------------
// server.listen(server.get('port'), function() {
//     loggs(server).Console("Node server running on http://localhost:" + server.get('port'));
// });

app.set('port', process.env.PORT || 3000);
var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});

// server.listen(3000, function() {
//     console.log("Node server running on http://localhost:3000");
// });
// ##### puerto de escucha del server ------------------------------------------ FIN
module.exports = server;