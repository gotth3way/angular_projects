// #########################
// #
// # GENERAL
// #
// #########################

function init_UIKIT() {
    // console.log("begin uikit");

    var $ = jQuery;
    var screenRes = $(window).width();
    var screenHeight = $(window).height();

    $("a").not('#download-apk').click(function(event) {
        event.preventDefault();
    });

    // Body Wrap
    $(".body_wrap").css("min-height", screenHeight);
    $(window).resize(function() {
        screenHeight = $(window).height();
        $(".body_wrap").css("min-height", screenHeight);
    });

    // Remove outline in IE
    $("a, input, textarea").attr("hideFocus", "true").css("outline", "none");
    // Add gradient to IE
    setTimeout(function() {
        $("input, textarea, .select_styled, .body_wrap, .boxed-velvet .inner, .widget_categories li, .dropdown > li a, .tabs li a, .tab-pane, .comment-body .inner, .chzn-container, .carousel-title, .note").addClass("gradient");
    }, 0);

    // First Child, Last Child
    $(".widget-container li:first-child, .pricing_box li:first-child, .dropdown li:first-child, ol li:first-child").addClass("first");
    $(".widget-container li:last-child, .pricing_box li:last-child, .dropdown li:last-child, ol li:last-child").addClass("last");

    // buttons
    $(".btn").not(".btn-round").not(".btn-black").hover(function() {
        $(this).stop().animate({ "opacity": 0.8 });
    }, function() {
        $(this).stop().animate({ "opacity": 1 });
    });
    $('a.btn, span.btn').on('mousedown', function() {
        $(this).addClass('active');
        console.log("addING active !!!!!!!!!!!!");
    });
    $('a.btn, span.btn').on('mouseup mouseout', function() {
        $(this).removeClass('active');
        console.log("REMOVING active !!!!!!!!!!!!");
    });

    // style Select, Radio, Checkbox
    // if ($("select").hasClass("select_styled")) {
    //     cuSel({ changedEl: ".select_styled", visRows: 10 });
    // }
    // if ($("div,p").hasClass("input_styled")) {
    //     $(".input_styled input").customInput();
    // }

    // NavBar Parents Arrow
    $(".dropdown ul").parent("li").addClass("parent");
    // NavBar
    $(".dropdown ul li:first-child, .cusel span:first-child").addClass("first");
    $(".dropdown ul li:last-child, .cusel span:last-child").addClass("last");

    // Tabs
    var $tabs_on_page = $('.tabs').length;
    var $bookmarks = 0;

    for (var i = 1; i <= $tabs_on_page; i++) {
        $('.tabs').eq(i - 1).addClass('tab_id' + i);
        $bookmarks = $('.tab_id' + i + ' li').length;
        $('.tab_id' + i).addClass('bookmarks' + $bookmarks);
    };

    $('.tabs li, .payment-form .btn').click(function() {
        setTimeout(function() {
            for (var i = 1; i <= $tabs_on_page; i++) {
                $bookmarks = $('.tab_id' + i + ' li').length;
                for (var j = 1; j <= $bookmarks; j++) {
                    $('.tab_id' + i).removeClass('active_bookmark' + j);

                    if ($('.tab_id' + i + ' li').eq(j - 1).hasClass('active')) {
                        $('.tab_id' + i).addClass('active_bookmark' + j);
                    }
                }
            }
        }, 0)
    });

    // Payment Form
    $('.payment-form #billing .btn, .payment-form #payment .btn-left').click(function() {
        $('a[href="#shipping"]').tab('show');
    });
    $('.payment-form #shipping .btn-left').click(function() {
        $('a[href="#billing"]').tab('show');
    });
    $('.payment-form #shipping .btn-red').click(function() {
        $('a[href="#payment"]').tab('show');
    });

    // Service List 2
    $('.service_list_2 .service_item').not(':even').addClass('even');
    $('.service_list_2 .service_item').not(':odd').addClass('odd');

    // prettyPhoto lightbox, check if <a> has atrr data-rel and hide for Mobiles
    if ($('a').is('[data-rel]') && screenRes > 600) {
        $('a[data-rel]').each(function() {
            $(this).attr('rel', $(this).data('rel'));
        });
        $("a[rel^='prettyPhoto']").prettyPhoto({ social_tools: false });
    };

    // Smooth Scroling of ID anchors
    function filterPath(string) {
        return string
            .replace(/^\//, '')
            .replace(/(index|default).[a-zA-Z]{3,4}$/, '')
            .replace(/\/$/, '');
    }
    var locationPath = filterPath(location.pathname);
    var scrollElem = scrollableElement('html', 'body');

    // $('a[href*=#].anchor').each(function() {
    //     $(this).click(function(event) {
    //         var thisPath = filterPath(this.pathname) || locationPath;
    //         if (locationPath == thisPath &&
    //             (location.hostname == this.hostname || !this.hostname) &&
    //             this.hash.replace(/#/, '')) {
    //             var $target = $(this.hash),
    //                 target = this.hash;
    //             if (target && $target.length != 0) {
    //                 var targetOffset = $target.offset().top;
    //                 event.preventDefault();
    //                 $(scrollElem).animate({ scrollTop: targetOffset }, 400, function() {
    //                     location.hash = target;
    //                 });
    //             }
    //         }
    //     });
    // });

    // use the first element that is "scrollable"
    function scrollableElement(els) {
        for (var i = 0, argLength = arguments.length; i < argLength; i++) {
            var el = arguments[i],
                $scrollElement = $(el);
            if ($scrollElement.scrollTop() > 0) {
                return el;
            } else {
                $scrollElement.scrollTop(1);
                var isScrollable = $scrollElement.scrollTop() > 0;
                $scrollElement.scrollTop(0);
                if (isScrollable) {
                    return el;
                }
            }
        }
        return [];
    };

    // // ##### Audio Player
    // var $players_on_page = $('.jp-audio').length;
    // var $song_title = '';

    // if ($players_on_page > 0) {
    //     console.log("players on page: " + $players_on_page);
    //     for (var i = 1; i <= $players_on_page; i++) {
    //         $('.jp-audio').eq(i - 1).addClass('jp-audio' + i);
    //     };

    //     setTimeout(function() {
    //         for (var i = 1; i <= $players_on_page; i++) {
    //             $song_title = $('.jp-audio' + i + ' .jp-playlist ul li.jp-playlist-current .jp-playlist-item').html();
    //             $('.jp-audio' + i + ' .song_title').html($song_title);
    //         };
    //     }, 500);

    //     function switchSong() {
    //         setTimeout(function() {
    //             for (var i = 1; i <= $players_on_page; i++) {
    //                 $('.jp-audio' + i + ' .jp-previous, .jp-audio' + i + ' .jp-next').removeClass('disabled');

    //                 if ($('.jp-audio' + i + ' .jp-playlist ul li:last-child').hasClass('jp-playlist-current')) {
    //                     $('.jp-audio' + i + ' .jp-next').addClass('disabled');
    //                 }
    //                 if ($('.jp-audio' + i + ' .jp-playlist ul li:first-child').hasClass('jp-playlist-current')) {
    //                     $('.jp-audio' + i + ' .jp-previous').addClass('disabled');
    //                 }
    //                 $song_title = $('.jp-audio' + i + ' .jp-playlist ul li.jp-playlist-current .jp-playlist-item').html();
    //                 $('.jp-audio' + i + ' .song_title').html($song_title);
    //             }
    //         }, 0)
    //     };

    //     $('.jp-previous, .jp-next, .jp-playlist ul').click(function() {
    //         switchSong()
    //     });
    //     if ($(".jp-jplayer")) {
    //         $(".jp-jplayer").on($.jPlayer.event.ended, function(event) {
    //             switchSong();
    //         });
    //     }
    // };

    // Placeholders
    setTimeout(function() {
        if ($.Placeholder) {
            $.Placeholder.init({ color: "#ededed" });
        }
    }, 0);

    // Scroll Bars
    var $scrolls_on_page = $('.scrollbar.style2').length;
    var $scroll_height = 0;

    for (var i = 1; i <= $scrolls_on_page; i++) {
        $('.scrollbar.style2').eq(i - 1).addClass('id' + i);
    };

    setTimeout(function() {
        $(".jspTrack").append("<div class='jspProgress'></div>");
        $(document).on('jsp-scroll-y', '.scrollbar.style2', function() {
            for (var i = 1; i <= $scrolls_on_page; i++) {
                $scroll_height = $('.scrollbar.style2.id' + i + ' .jspDrag').css('top');
                $('.scrollbar.style2.id' + i + ' .jspDrag').siblings(".jspProgress").css({ "height": parseInt($scroll_height, 10) + 10 + "px" });
            }
        });
    }, 0);

    // Rating Stars
    $(".rating span.star").hover(
        function() {
            $('.rating span.star').removeClass('on').addClass('off');
            $(this).prevAll().addClass('over');
        },
        function() {
            $(this).removeClass('over');
        }
    );

    $(".rating").mouseleave(function() {
        $(this).parent().find('.over')
            .removeClass('over');
    });

    $(".rating span.star").click(function() {
        $(this).prevAll().removeClass('off').addClass('on');
        $(this).removeClass('off').addClass('on');
    });

    // Crop Images in Image Slider

    // adds .naturalWidth() and .naturalHeight() methods to jQuery for retrieving a normalized naturalWidth and naturalHeight.
    (function($) {
        var
            props = ['Width', 'Height'],
            prop;

        while (prop = props.pop()) {
            (function(natural, prop) {
                $.fn[natural] = (natural in new Image()) ?
                    function() {
                        return this[0][natural];
                    } :
                    function() {
                        var
                            node = this[0],
                            img,
                            value;

                        if (node.tagName.toLowerCase() === 'img') {
                            img = new Image();
                            img.src = node.src,
                                value = img[prop];
                        }
                        return value;
                    };
            }('natural' + prop, prop.toLowerCase()));
        }
    }(jQuery));

    var
        carousels_on_page = $('.carousel-inner').length,
        carouselWidth,
        carouselHeight,
        ratio,
        imgWidth,
        imgHeight,
        imgRatio,
        imgMargin,
        this_image,
        images_in_carousel;

    for (var i = 1; i <= carousels_on_page; i++) {
        $('.carousel-inner').eq(i - 1).addClass('id' + i);
    }

    /* añade comportamientos de dudosa reputacion a  las imagenes del slider, si fallan lo capas abajo */
    function imageSize() {
        setTimeout(function() {
            for (var i = 1; i <= carousels_on_page; i++) {
                carouselWidth = $('.carousel-inner.id' + i + ' .item').width();
                carouselHeight = $('.carousel-inner.id' + i + ' .item').height();
                ratio = carouselWidth / carouselHeight; // 800/500

                images_in_carousel = $('.carousel-inner.id' + i + ' .item img').length;

                for (var j = 1; j <= images_in_carousel; j++) {
                    this_image = $('.carousel-inner.id' + i + ' .item img').eq(j - 1);
                    imgWidth = this_image.naturalWidth();
                    imgHeight = this_image.naturalHeight();
                    imgRatio = imgWidth / imgHeight;

                    if (ratio <= imgRatio) {
                        imgMargin = parseInt((carouselHeight / imgHeight * imgWidth - carouselWidth) / 2, 10);
                        this_image.css("cssText", "height: " + carouselHeight + "px; margin-left:-" + imgMargin + "px;");
                    } else {
                        imgMargin = parseInt((carouselWidth / imgWidth * imgHeight - carouselHeight) / 2, 10);
                        this_image.css("cssText", "width: " + carouselWidth + "px; margin-top:-" + imgMargin + "px;");
                    }
                }
            }
        }, 1000);
    };
    imageSize(); /* capar aqui en caso de fallo */
    $(window).resize(function() {
        $('.carousel-indicators .first').click();
        imageSize(); /* capar aqui en caso de fallo */
    });

}
// ############ PAGES: UIKIT

// ############ MENU 2 SLIDER
function init_MENUSLIDER() {
    // modificado: ################################# control slider y buttons nav !!! hay un tercero que se carga desde home
    // si pulsamos sobre los indicators de abajo
    $("#myCarousel .carousel-indicators li").click(function() {
        var data_button_to = parseInt($(this).attr('data-slide-to')); //numero entero del boton pulsado de lso indicators
        var buttons_nav = $('#menuslider a');
        for (i = 0; i < buttons_nav.length; i++) {
            if ($(buttons_nav[i]).hasClass('active')) {
                if (data_button_to !== i) {
                    $(buttons_nav[i]).removeClass('active');
                    $(buttons_nav[data_button_to]).addClass('active');
                    // console.log($(buttons_nav[data_button_to]).hasClass('active'));
                    $('#myCarousel').carousel(data_button_to);
                }
            } // else nothing todo
        }
    });
    // si pulsamos los botones laterales del slider
    $(".carousel-control.right").click(function() {
        carouselMove(1);
    });
    $(".carousel-control.left").click(function() {
        carouselMove(-1);
    });

    // si movemos slider a derecha o a izquierda
    function carouselMove(direccion) {
        var buttons_nav = $('#menuslider a'); // los botones del menu de arriba
        var indicators = $('#indicators li'); // los botones del slider abajo
        var data_button_to;
        for (i = 0; i < indicators.length; i++) {
            $(buttons_nav[i]).removeClass('active');
            if ($(indicators[i]).hasClass('active')) {
                data_button_to = i + direccion;
                if (data_button_to >= indicators.length) data_button_to = 0;
                if (data_button_to < 0) data_button_to = indicators.length - 1;
            }
        }
        $(buttons_nav[data_button_to]).addClass('active');
    }


    // si pulsamos directamente sobre los botones del menu de arriba
    $("#menuslider a").click(function() {
        var buttons_nav = $('#menuslider a'); // los botones del menu de arriba
        var indicators = $('#indicators li'); // los botones del slider abajo
        var data_button_to = parseInt($(this).attr('data-button')); // el numero del boton
        var items_carousel = $('#myCarousel .carousel-inner>.item');
        //limpiar tanto los active de los botones como de los indicators
        for (i = 0; i < indicators.length; i++) {
            $(buttons_nav[i]).removeClass('active');
            $(indicators[i]).removeClass('active');
            $(items_carousel[i]).removeClass('active')
        }
        $(indicators[data_button_to]).addClass('active'); // marcar los botones
        $(buttons_nav[data_button_to]).addClass('active'); // marcar los indicators
        $(items_carousel[data_button_to]).addClass('active'); // marcar los items del slider
    });

}
// ############ MENU 2 SLIDER

// ############ MAIN: SLIDER : VIDEO CONTROL
function init_VIDEO() {

    /*##### ANTIGUO REPRODUCTOR DE VIDEO DE JQUERY VIDEOJS DE UIKIT-VELVET (CACAFU) #####*/
    _VIDEO_(); // plugin _libs/uikit/video.js

    // _V_("my_video_1").ready(function() {
    //     // console.log(" funcionando ");

    //     var myPlayer = this;
    //     var carousel, widthC, heightC;
    //     var aspectRatio = 192 / 380;

    //     carousel = document.getElementById('myCarousel');
    //     widthC = carousel.clientWidth;
    //     heightC = carousel.clientHeight;
    //     console.log("video: ", widthC, heightC);

    //     // var width = document.getElementById(myPlayer.id).parentElement.offsetWidth;
    //     // console.log(width, widthC);
    //     // width = (width > 0) ? width : widthC;

    //     myPlayer.width(widthC).height(widthC * aspectRatio);

    //     // myPlayer.setVolume(1);

    //     function resizeVideoJS() {
    //         // var width = document.getElementById(myPlayer.id).parentElement.offsetWidth;
    //         widthC = carousel.clientWidth;
    //         heightC = carousel.clientHeight - 100;
    //         myPlayer.width(widthC).height(heightC); // myPlayer.width(widthC).height(widthC * aspectRatio);

    //     }
    //     // resizeVideoJS();
    //     window.onresize = resizeVideoJS;
    // });

    /*##### NUEVO REPRODUCTOR DE VIDEO ACORNMEDIAPLAYER DE JQUERY #####*/


    // <!-- ################ Acorn Media Player specific ENLACES AL CSS ##############-->
    // <link href="acornmediaplayer/acornmediaplayer.base.css" rel="stylesheet" type="text/css">
    // <!-- Themes -->
    // <link href="acornmediaplayer/themes/access/acorn.access.css" rel="stylesheet" type="text/css">
    // <link href="acornmediaplayer/themes/darkglass/acorn.darkglass.css" rel="stylesheet" type="text/css">
    // <link href="acornmediaplayer/themes/barebones/acorn.barebones.css" rel="stylesheet" type="text/css">

    // <!-- ################ ACCORN MEDIA PLAYER SPECIFIC HTML5 EXAMPLE ##############-->
    // <figure>
		// 	<video id="demo1" controls="controls" width="826" height="360" poster="website/images/tos-poster.jpg" preload="metadata" aria-describedby="full-descript">
		// 		<source type="video/webm" src="https://acornmedia.herokuapp.com/media/tears_of_steel_480.webm" />
		// 		<source type="video/mp4" src="https://acornmedia.herokuapp.com/media/tears_of_steel_480.mp4" />

		// 		<track src="subs/TOS-arabic.srt" kind="subtitles" srclang="ar" label="Arabic" />
		// 		<track src="subs/TOS-japanese.srt" kind="subtitles" srclang="jp" label="Japanese" />
		// 		<track src="subs/TOS-english.srt" kind="subtitles" srclang="en" label="English" />
		// 		<track src="subs/TOS-turkish.srt" kind="subtitles" srclang="tr" label="Turkish" />
		// 		<track src="subs/TOS-ukrainian.srt" kind="subtitles" srclang="uk" label="Ukrainian" />

		// 		You can download Tears of Steel at <a href="http://mango.blender.org/">mango.blender.org</a>.
		// 	</video>
		// 	<figcaption id="full-descript">
		// 		<p><em>"Tears of Steel"</em> was realized with crowd-funding by users of the open source 3D creation tool <a href="http://www.blender.org">Blender</a>. Target was to improve and test a complete open and free pipeline for visual effects in film - and to make a compelling sci-fi film in Amsterdam, the Netherlands. </p>
		// 		<p>(CC) Blender Foundation - <a href="http://www.tearsofsteel.org">http://www.tearsofsteel.org</a></p>
		// 	</figcaption>
		// </figure>

		// <figure>
		// 	<audio id="demo1-audio" aria-describedby="demo1-audio-descript">
		// 		<source type="audio/ogg" src="https://acornmedia.herokuapp.com/media/ptburnem-gypsyheartrock.ogg" />
		// 		<source type="audio/mp3" src="https://acornmedia.herokuapp.com/media/ptburnem-gypsyheartrock.mp3" />
		// 	</audio>
		// 	<figcaption id="demo1-audiodescript">
		// 		P.T.Burnem's track "Gypsy Heart Rock" from the "Paper Cranes" album.
		// 	</figcaption>
		// </figure>

    // <!-- ################ ACORN MEDIA PLAYER SPECIFIC ENLACES AL JS ###############-->
    //   <script src="acornmediaplayer/jquery.acornmediaplayer.js"></script>

    // <!-- Call the plugin -->
    // <script>
    // 	jQuery(function() {
    // 		// demo1
    		jQuery('#demo1, #demo1-audio').acornMediaPlayer({

          theme: 'access accesslight'
        });
    // 		// demo2
    // 		jQuery('#demo2, #demo2-audio').acornMediaPlayer({
    // 			theme: 'access accesslight'
    // 		});
    // 		// demo3
    // 		jQuery('#demo3, #demo3-audio').acornMediaPlayer({
    // 			theme: 'darkglass',
    // 			volumeSlider: 'vertical'
    // 		});
    // 		// demo4
    // 		jQuery('#demo4, #demo4-audio').acornMediaPlayer({
    // 			theme: 'darkglass darkglasssmall',
    // 			volumeSlider: 'vertical'
    // 		});
    // 		// demo5
    // 		jQuery('#demo5, #demo5-audio').acornMediaPlayer({
    // 			theme: 'barebones'
    // 		});
    // 		// demo6
    // 		jQuery('#demo6, #demo6-audio').acornMediaPlayer({
    // 			theme: 'barebones',
    // 			nativeSliders: true
    // 		});
    // 	});
    // </script>
}
// ############ MAIN: SLIDER :   VIDEO CONTROL

// ############ MAIN: SLIDER :   TRIPTICO
function init_TRIPTICO() {
    (function($) {
        $.fn.hoverfold = function(args) {
            this.each(function() {
                $(this).children('.view').each(function() {
                    var $item = $(this),
                        img = $item.children('img').attr('src'),
                        struct = '<div class="slice s1">';
                    struct += '<div class="slice s2">';
                    struct += '<div class="slice s3">';
                    struct += '<div class="slice s4">';
                    // struct += '<div class="slice s5">';
                    struct += '</div>';
                    struct += '</div>';
                    struct += '</div>';
                    struct += '</div>';
                    // struct += '</div>';
                    var $struct = $(struct);
                    $item.find('img').remove().end().append($struct).find('div.slice').css('background-image', 'url(' + img + ')').prepend($('<span class="overlay" ></span>'));
                });
            });
        };
    })(jQuery);
    $('#grid').hoverfold();


    $(function() {
        $('.btn-moleculle').click(function() {
            $('#wrap-center').hasClass('open') ? $('#wrap-center').removeClass('open') : $('#wrap-center').addClass('open');
        });
    });
}
// ############ MAIN: SLIDER :  TRIPTICO

// ############ DASHBOARD: PRODUCTS: TABS
function init_TABS() {
    var tab_buttons = $('.prd-tabs ul li a'); // botones tabs (ul li a)
    var tabs = $('.tabbed .tab-content'); // contenidos tab-content
    // console.log(tabs);
    var numItem;

    $(tab_buttons).click(function() {
        for (var i = 0; i < tab_buttons.length; i++) {
            if ($(tab_buttons[i]).hasClass('active')) {
                numItem = i;
            }
            console.log("numItem ", numItem, i);
            $(tab_buttons[i]).removeClass('active');
            $(tabs[i]).removeClass('active');

        }
        $(this).addClass('active');
        $(tabs[numItem]).addClass('active');
    });
}
// ############ DASHBOARD: PRODUCTS: TABS

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// ###################################################### inicio de scripts y plugins
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// solo se cargan una vez
// $(document).ready(function() {

// });


// ############################ PAGES: se cargan siempre en todas las paginas
function carga_UIKIT() {
    $(document).ready(function() {
        // ####################### init_UIKIT()
        init_UIKIT();
        // ###################### init_TRIANGLE
        init_TRIANGLE();
        // ####################### init_GMAP
        init_GMAP();
    });
}

// ############################# HOME: se cargan solo en home
function carga_PLUGINS_HOME() {
    $(document).ready(function() {
        // ###################### init_MENUSLIDER
        init_MENUSLIDER();
        // ###################### init_VIDEO
        init_VIDEO(); // este llama asu vez a SU PLUGIN _VIDEO_()
        // ###################### init_TRIPTICO
        init_TRIPTICO();
        // ###################### init_FBX
        // init_FBX();
        // ###################### init_PDB
        // init_PDB();
                // ###################### init_json
        // init_JSON();// NO FUNCIONA CON eltipo .json
        init_GLTF()
        // ###################### init_OBJ
        // init_OBJ();

    });
}

// ############################## PRODUCTS : solo en products
function carga_PLUGINS_PRODUCTS() {
    $(document).ready(function() {
        // ###################### init_TABS
        init_TABS();
    });
}
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// ###################################################### inicio de scripts y plugins
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
