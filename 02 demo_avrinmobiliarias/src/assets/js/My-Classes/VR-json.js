function init_JSON() {
  if (!Detector.webgl) Detector.addGetWebGLMessage();
  var container, carousel, stats, controls;
  var Objects = [{
            'name': 'inmueble',
            'camera_perspective': [45, 1, 2000],
            'camera_position': [0, 0, 100],
            'target_set': [0, 10, 0],
            'plane_geometry': [2000, 2000]
  }];
  // selleciona el objeto  aqui
  var elem = Objects[0];
  var n_json = elem.name;
  var cper = elem.camera_perspective;
  var cpos = elem.camera_position;
  var ts = elem.target_set;
  var pg = elem.plane_geometry;
  // console.log(elem);
  var camera, scene, renderer, light;
  var clock = new THREE.Clock();
  var mixers = [];
  // del archivo originales
  // var mouseX = 0, mouseY = 0;
  // var windowHalfX = window.innerWidth / 2;
  // var windowHalfY = window.innerHeight / 2;

  init();

  function init() {
    console.log("##### VR-json ##### iniciando");
    container = document.getElementById('json-container');
    carousel = document.getElementById('myCarousel');
    container.width = carousel.clientWidth;
    container.height = carousel.clientHeight;
    // console.log("fbx: ", container.width, container.height);
    camera = new THREE.PerspectiveCamera( cper[0], container.width / container.height, cper[1], cper[2] );
    // camera.position.z = 4;
    camera.position.set(cpos[0], cpos[1], cpos[2]);// camera_position(0, 100, 0)
    // scene
    controls = new THREE.OrbitControls(camera, container); // renderer.domElement
    controls.target.set(ts[0], ts[1], ts[2]); // controls.target.set(0, 100, 0);
    controls.update();
    scene = new THREE.Scene();
    scene.background = new THREE.Color(0xa0a0a0);
    scene.fog = new THREE.Fog(0xa0a0a0, 200, 1000);
    light = new THREE.HemisphereLight(0xffffff, 0x444444);
    light.position.set(0, 200, 0);
    scene.add(light);
    light = new THREE.DirectionalLight(0xffffff);
    light.position.set(0, 200, 100);
    light.castShadow = true;
    light.shadow.camera.top = 180;
    light.shadow.camera.bottom = -100;
    light.shadow.camera.left = -120;
    light.shadow.camera.right = 120;
    scene.add(light);

    // ground
    var mesh = new THREE.Mesh(new THREE.PlaneGeometry(pg[0], pg[1]), new THREE.MeshPhongMaterial({ color: 0x999999, depthWrite: false })); //var mesh = new THREE.Mesh(new THREE.PlaneGeometry(2000, 2000), new THREE.MeshPhongMaterial({ color: 0x999999, depthWrite: false }));
    mesh.rotation.x = -Math.PI / 2;
    mesh.receiveShadow = true;
    scene.add(mesh);
    var grid = new THREE.GridHelper(2000, 20, 0x000000, 0x000000);
    grid.material.opacity = 0.2;
    grid.material.transparent = true;
    scene.add(grid);

    // BEGIN model JSON loader code
    var objectLoader = new THREE.ObjectLoader();
    objectLoader.load( "../assets/My-Objects/3D_Models/json/" + n_json + '.json', function ( obj, materials ) {
      console.log("##### VR-json ##### cargando el objeto ...");
      loader.onLoadComplete = function(){
            object.rotation.x = - (Math.PI / 2);
            object.rotation.y = Math.PI;
            object.scale.set(.025, .025, .025);
            object.position.set(0, 1, .4);

        scene.add( obj );
        console.log("##### VR-json ##### cargado el objeto");
      } //scene.add( obj );
    }, undefined, function (e) {
        console.error(e);
    } );
    // END Clara.io JSON loader code

    //renderer
    console.log("##### VR-json ##### renderizando");
    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(container.width, container.height);
    renderer.shadowMap.enabled = true;
    container.appendChild(renderer.domElement);
    console.log("##### VR-json ##### renderizado");

    // resizing
    window.addEventListener('resize', onWindowResize, false);

    // renderer = new THREE.WebGLRenderer();
    // renderer.setPixelRatio( window.devicePixelRatio );
    // renderer.setSize( window.innerWidth, window.innerHeight );
    // container.appendChild( renderer.domElement );
    // document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    animate();
  }


  function onWindowResize() {
    console.log("##### VR-json ##### redimensionando la ventana");
    // windowHalfX = window.innerWidth / 2;
    // windowHalfY = window.innerHeight / 2;
    // camera.aspect = window.innerWidth / window.innerHeight;
    // camera.updateProjectionMatrix();
    // renderer.setSize( window.innerWidth, window.innerHeight );

    container.width = carousel.clientWidth; // window.innerWidth || 800;
    container.height = carousel.clientHeight; // window.innerHeight 7 2 || 500;
    // console.log("container-fbx: ", container.width, container.height);
    camera.aspect = container.width / container.height;
    camera.updateProjectionMatrix();
    renderer.setSize(container.width, container.height);
    console.log("##### VR-json ##### redimensionada la ventana");
  }
  // function onDocumentMouseMove( event ) {
  //   mouseX = ( event.clientX - windowHalfX ) / 2;
  //   mouseY = ( event.clientY - windowHalfY ) / 2;
  // }
  //
  function animate() {
    // requestAnimationFrame( animate );
    // render();

    requestAnimationFrame(animate);
        if (mixers.length > 0) {
            for (var i = 0; i < mixers.length; i++) {
                mixers[i].update(clock.getDelta());
            }
        }
        renderer.render(scene, camera);
  }
  // function render() {
  //   camera.position.x += ( mouseX - camera.position.x ) * .05;
  //   camera.position.y += ( - mouseY - camera.position.y ) * .05;
  //   camera.lookAt( scene.position );
  //   renderer.render( scene, camera );
  // }
}
