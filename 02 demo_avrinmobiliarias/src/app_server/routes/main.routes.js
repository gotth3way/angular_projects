var express = require('express');

var app = express();
/* ######## RUTA RAIZ: MAIN ########## */
app.get('/', (req, res) => {
    res.status(200).json({
        ok: true,
        message: "estamos en ruta /"
    });
});
/* ######## RUTA RAIZ: MAIN ########## */

module.exports = app;