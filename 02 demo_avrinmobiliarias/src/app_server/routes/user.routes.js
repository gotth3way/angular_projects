var express = require('express');
var app = express();
// requires
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var SEED = require('../_configs/general').TOKEN_SECRET;
// midlewares
var mdValidate = require('../midlewares/validate');
var mdAuthenticate = require('../midlewares/authenticate.js');
// model
var User = require('../models/user.model.js');
/* ######## RUTA user ########## */
// ===============================
// GET: obtener toso los usuarios  //quizás esto no haga falta
// ===============================
app.get('/', (req, res) => {

    User.find({}, { _id: 1, name: 1, email: 1, url_photo: 1, provider: 1 })
        .exec((err, users) => {
            if (err) {
                res.status(500).json({
                    ok: false,
                    message: "ERROR: bd falló!",
                    errors: err
                });
            } else {
                res.status(200).json({
                    ok: true,
                    users: users
                });
            }
        });

});
// ===============================
// POST: subir un usuario nuevo quizas en el register
// ===============================
app.post('/', mdValidate.verifyNewUser, (req, res) => {
    var body = req.body;
    // ############################################################# encryptamos
    var salt = bcrypt.genSaltSync(10); // ertorna un salt
    var password = bcrypt.hashSync(body.password, salt); // crea un  hash
    // ############################################################# encryptamos
    // preparamos los datos
    var user = new User({
        name: body.name,
        email: body.email,
        salt: salt,
        password: password,
    });
    // grabamos el user
    user.save((err, userBD) => {
        if (err) {
            res.status(500).json({
                ok: false,
                message: "fallo al intentar grabar",
                errors: err
            });
        } else {
            // filtrar datos a enviar
            userBD.password = ":)";
            userDB.salt = ":(";
            // ############# !!! IMPORTANT enviar token aquí y quizás colocar en req.user ese user
            var token = jwt.sign({ user: userDB }, SEED, { expiresIn: 43200 });
            req.user = userDB;
            // ############# !!! IMPORTANT enviar token aquí y quizás colocar en req.user ese user
            res.status(200).json({
                ok: true,
                message: "el usuario fue registrado",
                user: userBD,
                token: token,
                id: userBD._id
            });
        }
    });
});
// ===============================
// UPDATE: actualizar usuario
// ===============================
app.put('/:id', [mdAuthenticate.verifyToken, mdValidate.verifyUser], (req, res) => {
    var body = req.body;
    var id = req.params.id;

    User.findById(id, (err, userBD) => {
        if (err) {
            res.status(500).json({
                ok: false,
                message: "error al buscar el usuario",
                errors: err
            });
        }
        if (!userBD) {
            res.status(400).json({
                ok: false,
                message: "El usuario con el id " + id + ", no existe",
                errors: { message: 'no existe un usario con ese id' }
            });
        }

    });
    // validado y existe
    var name = body.name;
    var email = body.email;
    // actualizamos el user
    User.update({ _id: id }, {
        $set: {
            name: name,
            email: email
        }
    }, (err, respMongo) => {
        if (err) {
            res.status(500).json({
                ok: false,
                message: "fallo al intentar grabar",
                errors: err
            });
        } else {
            // tenemos el user del find solo hay que actualizarlo
            res.status(200).json({
                ok: true,
                message: "el usuario fue actualizado",
                resp: respMongo
            }); // si lo que quieres es el usuario completo haces un finbyid o en vez de update usas save()
        }
    });
});
// ===============================
// DELETE: borrar usuario !!!WARNING NO DEBES PERMITIR BORRAR USERS
// ===============================
app.delete('/:id', [mdAuthenticate.verifyToken, mdValidate.verifyId], (req, res) => {

    var id = req.params.id;

    User.findByIdAndRemove(id, (err, userDeleted) => {
        if (err) {
            res.status(500).json({
                ok: false,
                message: "fallo al intentar borrar",
                errors: err
            });
        } else if (!userDeleted) {
            res.status(400).json({
                ok: false,
                message: "no existe usuario con ese id",
                errors: { message: "el id no existe" }
            });
        } else {
            // tenemos el user del find solo hay que actualizarlo
            res.status(200).json({
                ok: true,
                message: "el usuario fue borrado",
                userDeleted: userDeleted
            }); // si lo que quieres es el usuario completo haces un finbyid o en vez de update usas save()
        }
    });

});

/* ######## RUTA user ########## */

module.exports = app;