import { RouterModule, Routes } from '@angular/router';

// PAGES


import { HomeComponent } from './structure/pages/home/home.component';
import { ProductsComponent } from './structure/pages/products/products.component';
import { BlogComponent } from './structure/pages/blog/blog.component';
import { ContactComponent } from './structure/pages/contact/contact.component';
import { AboutComponent } from './structure/pages/about/about.component';
import { PagesComponent } from './structure/pages/pages.component';
import { AppComponent } from './app.component';





const appRoutes: Routes = [
  {path: '**', component: PagesComponent },
  // {path: '**', component: NotfoundComponent },

];

export const APP_ROUTES = RouterModule.forRoot( appRoutes, { useHash: true });
