import { Component, OnInit } from '@angular/core';


// declare function carga_PLUGINS();
declare function carga_UIKIT();

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: []
})
export class PagesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // carga_PLUGINS();
    carga_UIKIT();
  }

}
