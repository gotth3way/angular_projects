import { Component, OnInit } from '@angular/core';

declare function carga_PLUGINS_HOME();


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    carga_PLUGINS_HOME();
  }

}
