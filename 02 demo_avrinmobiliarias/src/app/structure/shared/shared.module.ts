import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// material imports
import { MatMenuModule, MatToolbarModule, MatTabsModule, MatButtonModule, MatIconModule, MatCardModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// shareds
  import { HeaderComponent } from './header/header.component';
  import { DashboardComponent } from './dashboard/dashboard.component';
    import { MenuSliderComponent } from '../pages/home/menu-slider/menu-slider.component';
    import { MainComponent } from '../pages/home/main/main.component';
    import { AsideComponent } from '../pages/home/aside/aside.component';
  import { FooterComponent } from './footer/footer.component';
  import { NotfoundComponent } from '../../notfound/notfound.component';
  import { PAGES_ROUTES } from '../pages/pages.routes';



@NgModule({
  imports: [
    PAGES_ROUTES,
    MatMenuModule,
    MatToolbarModule,
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    BrowserAnimationsModule
  ],
  declarations: [
    HeaderComponent,
    DashboardComponent,
    MenuSliderComponent,
    MainComponent,
    AsideComponent,
    FooterComponent,
    NotfoundComponent
  ],
  exports: [
    HeaderComponent,
    DashboardComponent,
    MenuSliderComponent,
    MainComponent,
    AsideComponent,
    FooterComponent,
    NotfoundComponent,
    MatMenuModule,
    MatToolbarModule,
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    BrowserAnimationsModule
  ]
})
export class SharedModule { }
