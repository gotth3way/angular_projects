// ############################# modulos
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
// ##### MY MODULES
import { PagesModule } from './structure/pages/pages.module';
// ############################# modulos
// ############################# components
// app imports
import { AppComponent } from './app.component';
// RUTAS
import { APP_ROUTES } from './app.routes';
// ############################# components

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    APP_ROUTES,
    PagesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
